.POSIX:

CC = gcc
CFLAGS = -std=c11 -pedantic -Wall -Wextra
LDFLAGS = -s -static

bins = \
    from \
    glob \
    math \
    read

all: $(bins)

math: math.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $@.c -lm

clean:
	rm -f $(bins)
