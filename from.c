/*
 * from.c: External implementation of read process substitution
 *
 * Usage: from <command> [<args>...]
 *
 * Example process substitution in bash:
 *     cat <(echo hello world)
 * Using from:
 *     cat "$(from echo hello world)"
 */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdnoreturn.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

static char *fifopath;
static int doclean = 1;

static noreturn void
die(char *func)
{
	perror(func);
	if (!doclean)
		_exit(1);
	if (fifopath)
		unlink(fifopath);
	exit(1);
}

int
main(int argc, char *argv[])
{
	if (!(fifopath = tmpnam(0)))
		die("tmpnam");
	if (mkfifo(fifopath, 0600))
		die("mkfifo");
	if (puts(fifopath) == EOF)
		die("puts");
	if (fflush(stdout) == EOF)
		die("fflush");

	/* first fork: parent process returns so command substitution finishes */
	switch (fork()) {
		case -1:
			die("fork");
		case 0:
			break;
		default:
			return 0;
	}

	if (fclose(stdout))
		die("fclose");

	/* second fork: child execs command and parent waits for cleanup */
	switch (doclean = fork()) {
		case -1:
			die("fork");
		case 0:
			if (!freopen(fifopath, "w", stdout))
				die("freopen");
			if (argc < 2)
				_exit(0);
			execvp(argv[1], argv + 1);
			die("exec");
		default:
			if (wait(0) < 0)
				die("wait");
			unlink(fifopath);
			return 0;
	}
}
