#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdnoreturn.h>

typedef union {
	char                c;
	signed char        sc;
	unsigned char      uc;
	short              ss;
	unsigned short     us;
	int                si;
	unsigned int       ui;
	long               sl;
	unsigned long      ul;
	long long          sll;
	unsigned long long ull;
	float               f;
	double              d;
	long double        ld;
	int8_t             i8;
	uint8_t            u8;
	int16_t            i16;
	uint16_t           u16;
	int32_t            i32;
	uint32_t           u32;
	int64_t            i64;
	uint64_t           u64;
} Val;

typedef struct {
	Val (*add)(Val, Val);
	Val (*sub)(Val, Val);
	Val (*mul)(Val, Val);
	Val (*div)(Val, Val);
	Val (*mod)(Val, Val);
	Val (*pow)(Val, Val);

	Val (*and)(Val, Val);
	Val (*or )(Val, Val);
	Val (*xor)(Val, Val);
	Val (*not)(Val);
	Val (*sl )(Val, Val);
	Val (*sr )(Val, Val);

	Val (*eq )(Val, Val);
	Val (*ne )(Val, Val);
	Val (*gt )(Val, Val);
	Val (*ge )(Val, Val);
	Val (*lt )(Val, Val);
	Val (*le )(Val, Val);
} Ops;

static noreturn void
die(char *str)
{
	fputs(str, stderr);
	fputc('\n', stderr);
	exit(1);
}

#define BINOP(op, name, type, member) static Val member##_##name(Val a, Val b) { return (Val){ .member = a.member op b.member   }; }
#define UNAOP(op, name, type, member) static Val member##_##name(Val a)        { return (Val){ .member = op a.member            }; }
#define BINFN(fn, name, type, member) static Val member##_##name(Val a, Val b) { return (Val){ .member = fn(a.member, b.member) }; }
#define BINERR(name, type, member) static noreturn Val member##_##name(Val a, Val b) { die("Unsupported operation " #name " on type " #type); }
#define UNAERR(name, type, member) static noreturn Val member##_##name(Val a) { die("Unsupported operation " #name " on type " #type); }

#define DEF_ARITHMETIC_OPS(type, member) \
BINOP(+, add, type, member) \
BINOP(-, sub, type, member) \
BINOP(*, mul, type, member) \
BINOP(/, div, type, member)

#define DEF_EQUALITY_OPS(type, member) \
BINOP(==, eq , type, member) \
BINOP(!=, ne , type, member) \
BINOP(> , gt , type, member) \
BINOP(>=, ge , type, member) \
BINOP(< , lt , type, member) \
BINOP(<=, le , type, member)

#define DEF_BITWISE_OPS(type, member) \
BINOP(& , and, type, member) \
BINOP(| , or , type, member) \
BINOP(^ , xor, type, member) \
UNAOP(~ , not, type, member) \
BINOP(<<, sl , type, member) \
BINOP(>>, sr , type, member)

#define OPS(type, member) \
Ops member##_ops = { \
.add = member##_add, \
.sub = member##_sub, \
.mul = member##_mul, \
.div = member##_div, \
.mod = member##_mod, \
.pow = member##_pow, \
.and = member##_and, \
.or  = member##_or , \
.xor = member##_xor, \
.not = member##_not, \
.sl  = member##_sl , \
.sr  = member##_sr , \
.eq  = member##_eq , \
.ne  = member##_ne , \
.gt  = member##_gt , \
.ge  = member##_ge , \
.lt  = member##_lt , \
.le  = member##_le , \
};

#define DEF_INT_OPS(type, member) \
DEF_ARITHMETIC_OPS(type, member) \
DEF_EQUALITY_OPS(type, member) \
DEF_BITWISE_OPS(type, member) \
BINOP(%, mod, type, member) \
BINERR(pow, type, member) \
OPS(type, member)

#define DEF_FLOAT_OPS(type, member, suffix) \
DEF_ARITHMETIC_OPS(type, member) \
DEF_EQUALITY_OPS(type, member) \
BINERR(and, type, member) \
BINERR(or , type, member) \
BINERR(xor, type, member) \
UNAERR(not, type, member) \
BINERR(sl , type, member) \
BINERR(sr , type, member) \
BINFN(fmod##suffix, mod, type, member) \
BINFN(pow##suffix, pow, type, member) \
OPS(type, member)

/*
 * TODO:
 * custom functions to avoid signed integer overflow/underflow
 * custom integer pow() functions
 */

DEF_INT_OPS(char              ,  c )
DEF_INT_OPS(signed char       , sc )
DEF_INT_OPS(unsigned char     , uc )
DEF_INT_OPS(short             , ss )
DEF_INT_OPS(unsigned short    , us )
DEF_INT_OPS(int               , si )
DEF_INT_OPS(unsigned int      , ui )
DEF_INT_OPS(long              , sl )
DEF_INT_OPS(unsigned long     , ul )
DEF_INT_OPS(long long         , sll)
DEF_INT_OPS(unsigned long long, ull)
DEF_INT_OPS(int8_t            , i8 )
DEF_INT_OPS(uint8_t           , u8 )
DEF_INT_OPS(int16_t           , i16)
DEF_INT_OPS(uint16_t          , u16)
DEF_INT_OPS(int32_t           , i32)
DEF_INT_OPS(uint32_t          , u32)
DEF_INT_OPS(int64_t           , i64)
DEF_INT_OPS(uint64_t          , u64)

DEF_FLOAT_OPS(float      , f , f)
DEF_FLOAT_OPS(double     , d ,  )
DEF_FLOAT_OPS(long double, ld, l)


int
main(void)
{
	Val a = { .d = 2 }, b = { .d = 3 }, c = d_ops.pow(a, b);
	printf("%f\n", c.d);
	return 0;
}
