/*
 * read.c: Read a single line of input from stdin and print to stdout
 */
#include <stdio.h>

int
main(void)
{
	int c;
	while ((c = getchar()) != EOF) {
		if (putchar(c) == EOF)
			break;
		if (c == '\n')
			return 0;
	}
	if (ferror(stdin))
		perror("getchar");
	if (ferror(stdout))
		perror("putchar");
	return 1;
}
