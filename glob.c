/*
 * glob.c: External implementation of globbing
 *
 * Usage: glob <pattern>...
 *
 * Prints a nul separated list of file names that match the glob patterns.
 *     glob '*.c'
 * Is similar to:
 *     find . -name \*.c -exec printf %s\\0 {} +
 */
#include <stdio.h>
#include <glob.h>

int
main(int argc, char *argv[])
{
	while (*++argv) {
		glob_t paths;
		glob(*argv, 0, 0, &paths);
		for (size_t i = 0; i < paths.gl_pathc; i++) {
			fputs(paths.gl_pathv[i], stdout);
			fputc(0, stdout);
		}
		globfree(&paths);
	}
	return 0;
}
